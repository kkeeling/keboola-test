import csv
import sys
import traceback

try:
    with open("/data/in/tables/beta-users.csv", mode='rt', encoding='utf-8') as in_file, open("/data/out/tables/result.csv", mode='wt', encoding='utf-8') as out_file:
        writer = csv.DictWriter(out_file, fieldnames=['mlb_name', 'mlb_team'], dialect='excel')
        writer.writeheader()

        lazy_lines = (line.replace('\0', '') for line in in_file)
        csv_reader = csv.DictReader(lazy_lines, dialect='kbc')
        for row in csv_reader:
            name = row['mlb_name']
            team = row['mlb_team']
            out_row = {
                'mlb_name': name,
                'mlb_team': team
            }
            writer.writerow(out_row)
except ValueError as err:
    print(err, file=sys.stderr)
    sys.exit(1)
except Exception as err:
    print(err, file=sys.stderr)
    traceback.print_exc(file=sys.stderr)
    sys.exit(2)